package com.Experiment.ExtensionClass;

import manifold.ext.rt.api.Extension;
import manifold.ext.rt.api.This;

@Extension
public class StringExt {
    
    public static void Print(@This String thiz){
        System.out.println(thiz);
    }

}
