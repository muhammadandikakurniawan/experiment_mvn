package com.Experiment.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("Template1")
public class Template1Controller {
    
    @GetMapping("template1")
    public ModelAndView template1(){
        ModelAndView mv = new ModelAndView("Template1View/template1");
        // mv.setViewName("Template1View/template1");
        mv.addObject("header", "View Template 1");
        return mv;
    }

}
