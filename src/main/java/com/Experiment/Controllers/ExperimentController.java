package com.Experiment.Controllers;

import java.util.Arrays;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("Experiment")
public class ExperimentController {
    
    @Autowired
    private HttpServletResponse _response;

    @Autowired
    private HttpServletRequest _request;

    @RequestMapping(value = "SetCookie", method = RequestMethod.GET)
    public Mono<String> SetCookie(){

        Arrays.asList(this._request.getCookies()).stream().forEach(x -> {
            System.out.println(String.format("%s => %s", x.getName(), x.getValue()));
        });

        // Cookie cookie = new Cookie("cookie_service","kashdshdkajshdkasjhdkasjhdsj");
        // cookie.setMaxAge(5000);
        // _response.addCookie(cookie);
        return Mono.just("cookie setted");
    }

}
